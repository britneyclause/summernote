<?php

namespace Bolt\Extension\Bamcocreate\Summernote;

use Bolt\Storage\Field\Type\FieldTypeBase;
use Bolt\Storage\Field\Sanitiser\SanitiserAwareInterface;
use Bolt\Storage\Field\Sanitiser\SanitiserAwareTrait;
use Bolt\Storage\Field\Sanitiser\WysiwygAwareInterface;
use Doctrine\DBAL\Types\Type;

class SummernoteField extends FieldTypeBase implements SanitiserAwareInterface, WysiwygAwareInterface
{
    use SanitiserAwareTrait;

    public function getName()
    {
        return 'html2';
    }

    public function hydrate($data, $entity)
    {
        parent::hydrate($data, $entity);
    }

    public function getTemplate()
    {
        return '@bolt/_summernote.twig';
    }

    public function getStorageType()
    {
        return Type::getType('text');
    }
}