<?php
namespace Bolt\Extension\Bamcocreate\Summernote;

use Bolt\Asset\File\JavaScript;
use Bolt\Asset\File\Stylesheet;
use Bolt\Controller\Zone;
use Bolt\Extension\SimpleExtension;

class SummernoteExtension extends SimpleExtension
{
    public function registerFields()
    {
        return [
            new SummernoteField(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    protected function registerTwigPaths()
    {
        return [
            'templates' => ['position' => 'prepend', 'namespace' => 'bolt']
        ];
    }

    protected function registerAssets()
    {
        return [
            Stylesheet::create('summernote-lite.css')->setZone(Zone::BACKEND),
            JavaScript::create('summernote-lite.js')->setZone(Zone::BACKEND)->setPriority(10),
            JavaScript::create('start.js')->setZone(Zone::BACKEND)->setPriority(15),
        ];
    }
}